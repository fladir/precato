<?php get_header(); ?>

    <!-- Topo -->
<?php get_template_part('components/topo-da-pagina/topo-da-pagina');
$featured_img_url = get_the_post_thumbnail_url('full'); ?>
    <a class="link-imagem-portfolio-principal"
       href="<?php the_post_thumbnail_url(); ?>" data-toggle="lightbox"
       data-gallery="example-gallery">
        <figure class="figure">
            <?php the_post_thumbnail('portfolio_big', array('class' => 'img-portfolio-single my-5 py-5', 'alt' => '' . get_the_title() . '', 'title' => '' . get_the_title() . '')); ?>
                <i class="fas fa-search"></i>

        </figure>
    </a>
    <!-- Seção Single -->
<?php get_template_part('components/portfolio/secao-single'); ?>

    <!-- Call to action -->
<?php get_template_part('components/call-to-action/cta'); ?>

    <!-- Seções Alternadas -->
<?php get_template_part('components/portfolio/secoes-alternadas'); ?>

    <!-- Depoimentos -->
<?php get_template_part('components/depoimentos/depoimentos'); ?>

    <!-- Call to action -->
<?php get_template_part('components/call-to-action/cta'); ?>

    <!-- Newsletter -->
<?php get_template_part('components/newsletter/newsletter'); ?>

<?php get_footer(); ?>