<?php
$grupoFooter = get_fields('options')['grupo_footer'];
#echo '<pre>'; print_r($grupoFooter); echo'</pre>';
?>

<!-- Footer -->
<footer class="font-small text-white">
    <div class="container text-left wrapper-footer">

        <div class="row">

            <div class="col-md-3 logo-footer d-flex align-items-center justify-content-center ">
                <a href="<?php bloginfo('url'); ?>">
                    <img src="<?php echo $grupoFooter['logo_footer']['sizes']['logo_footer'] ?>" alt="Antecipação de Precatórios Federais - Precato">
                </a>
            </div>

            <div class="col-md-3 menu">
                <?php

                wp_nav_menu(array(
                    'theme_location' => 'secondary',
                    'depth' => 1,
                    'container' => 'div',
                    'container_class' => 'footer-navbar',
                    'container_id' => 'navFooter',
                    'menu_class' => 'nav navbar-nav navbar-desktop',
                    'fallback_cb' => 'WP_Bootstrap_Navwalker::fallback',
                    'walker' => new WP_Bootstrap_Navwalker(),
                ));

                ?>
            </div>

            <div class="col-md-3 enderecos">
            <?php $telEnd1 = $grupoFooter['endereco_1']['telefones'] ?>
                <p><b><?php echo $grupoFooter['endereco_1']['cidade_e_estado'] ?></b></p>
                <p><?php echo $grupoFooter['endereco_1']['nome_do_local'] ?></p>
                <p><?php echo $grupoFooter['endereco_1']['endereco'] ?></p>
                <?php if($telEnd1) : foreach ($telEnd1 as $tel1) : ?>
                    <a href="tel:<?php echo $tel1['telefone'] ?>"><p><?php echo $tel1['telefone'] ?></p></a>
                <?php endforeach; endif; ?>
                <p><?php echo $grupoFooter['endereco_1']['texto_adicional'] ?></p>
            </div>

            <div class="col-md-3 enderecos">
            <?php $telEnd2 = $grupoFooter['endereco_2']['telefones'] ?>
                <p><b><?php echo $grupoFooter['endereco_2']['cidade_e_estado'] ?></b></p>
                <p><?php echo $grupoFooter['endereco_2']['nome_do_local'] ?></p>
                <p><?php echo $grupoFooter['endereco_2']['endereco'] ?></p>
                <?php if($telEnd2) : foreach ($telEnd2 as $tel2) : ?>
                    <a href="tel:<?php echo $tel2['telefone'] ?>"><p><?php echo $tel2['telefone'] ?></p></a>
                <?php endforeach; endif; ?>
                <p><?php echo $grupoFooter['endereco_2']['texto_adicional'] ?></p>
            </div>

        </div>

    </div>

</footer>
<div class="mobile-menu-overlay"></div>

</div>

<?php wp_footer() ?>

</body>

</html>