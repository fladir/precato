<?php get_header(); ?>
    <!-- Topo -->
<?php  get_template_part('components/topo-da-pagina/topo-da-pagina'); ?>

    <section id="404">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <h2 class="titulo-destaque text-center duplicate" title="Confira Nossas Novidades">Confira Nossas Novidades</h2>
                </div>
            </div>
        </div>
    </section>
<?php get_template_part('/components/blog/blog'); ?>
<?php get_template_part('/components/newsletter/newsletter'); ?>
<?php get_footer(); ?>