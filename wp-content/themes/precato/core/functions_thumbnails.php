<?php
// Enable post thumbnails
add_theme_support('post-thumbnails');

// Custom image sizes
if (function_exists('add_image_size')) {
    add_image_size('logo_header', 170, 57); //Logo Header
    add_image_size('banner', 1920, 740); // Imagem Banner
    add_image_size('imagens_col6', 960, 590, true); // Imagens metade da página
    add_image_size('img_chamada_quem_somos', 211, 263); // Imagem Chamada Quem Somos
    add_image_size('img_chamada_2', 1920, 390); // Imagem Chamada 2
    add_image_size('parceiros', 200, 50); // Parceiros
    add_image_size('depoimentos', 200, 200, true); // Depoimentos
    add_image_size('logo_footer', 160, 30); // Logo Footer
    add_image_size('gal_quem_somos', 1140, 600); // Galeria Seções pág. Quem Somos
    add_image_size('icone_passos', 100, 100); // Ícone Passos pág. Precatórios
}