<?php

function pbo_load_js()
{       
    # wp_enqueue_script('loadingJS', get_template_directory_uri() . '/core/components/loading/loading.js', 1, false);  

    
    # Custom JS
    wp_enqueue_script('Bootstrap', get_template_directory_uri() . '/node_modules/bootstrap/dist/js/bootstrap.min.js', array('jquery'), 1, true);
    wp_enqueue_script('jQuery-Easing', get_template_directory_uri() . '/node_modules/jquery.easing/jquery.easing.js', array('jquery'), 1, true);
    wp_enqueue_script('Waypoint', get_template_directory_uri() . '/node_modules/waypoints/lib/jquery.waypoints.min.js', array('jquery'), 1, true);
    wp_enqueue_script('FontAwesome', get_template_directory_uri() . '/node_modules/@fortawesome/fontawesome-free/js/all.min.js', array('jquery'), 1, true);   
    wp_enqueue_script('owlCarousel-Js', get_template_directory_uri() . '/node_modules/owl.carousel/dist/owl.carousel.min.js', array('jquery'), 1, true);     
    wp_enqueue_script('WOW-Js', get_template_directory_uri() . '/node_modules/wowjs/dist/wow.min.js', array('jquery'), 1, true);   
    wp_enqueue_script('MakedInput-Js', get_template_directory_uri() . '/node_modules/jquery-mask-plugin/dist/jquery.mask.min.js', array('jquery'), 1, true);
    wp_enqueue_script('inView', get_template_directory_uri() . '/node_modules/in-view/dist/in-view.min.js', array('jquery'), 1, true);
    wp_enqueue_script('appearJs', get_template_directory_uri() . '/node_modules/jquery-appear-original/index.js', array('jquery'), 1, true);
    wp_enqueue_script('GMaps', '//maps.googleapis.com/maps/api/js?key=AIzaSyCxQHlF-7oXndl6laoEGZCzQ9kho7HhJzQ', array('jquery'), 1, true);
    wp_enqueue_script('ekkoLightbox', get_template_directory_uri() . '/node_modules/ekko-lightbox/dist/ekko-lightbox.min.js', array('jquery'), 1, true);
    wp_enqueue_script('resizeSensor', get_template_directory_uri() . '/node_modules/theia-sticky-sidebar/dist/ResizeSensor.min.js', array('jquery'), 1, true);
    wp_enqueue_script('stickySidebar', get_template_directory_uri() . '/node_modules/theia-sticky-sidebar/dist/theia-sticky-sidebar.min.js', array('jquery'), 1, true);
    wp_enqueue_script('fancyBox', get_template_directory_uri() . '/node_modules/@fancyapps/fancybox/dist/jquery.fancybox.min.js', array('jquery'), 1, true);
    wp_enqueue_script('Global-Js', get_template_directory_uri() . '/assets/js/global.js', array('jquery'), 1, true);



    wp_enqueue_script('utilsCore', get_template_directory_uri() . '/core/assets/js/utilsFramework.js', array('jquery'), 1, true);     
}

add_action('wp_enqueue_scripts', 'pbo_load_js');
