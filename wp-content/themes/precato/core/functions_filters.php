<?php

function pbo_custom_wp_title($title, $sep)
{
    global $paged, $page;

    if (is_feed()) {
        return $title;
    }

    // Adicionando o nome do site
    $title = get_field('grupo_header', 'options')['nome_da_empresa'] ? get_field('grupo_header', 'options')['nome_da_empresa'] : get_bloginfo('name');
    $slogan = get_field('grupo_header', 'options')['slogan'] ? get_field('grupo_header', 'options')['slogan'] : get_bloginfo('description');

    #if ($paged >= 2 || $page >= 2)
    #    $title = "$title $sep " . sprintf('Page %s', max($paged, $page));

    if(is_home() || is_front_page()){
        $title .= " $sep " . $slogan;
    } elseif(is_search()) {
        $title .= " $sep " . 'Resultado da busca';
    } elseif (is_tax()) {
        $title .= " $sep ". get_queried_object()->name;
    } elseif (is_404()) {
        $title .= " $sep " . 'Página não encontrada';
    } else {
        $title .= " $sep " . get_the_title();
    }

    return $title;
}
add_filter('wp_title', 'pbo_custom_wp_title', 10, 2);

add_filter('show_admin_bar', '__return_false');
