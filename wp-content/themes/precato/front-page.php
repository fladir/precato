<?php get_header() ?>

    <!-- Banner -->
<?php get_template_part('components/homepage/banner/banner'); ?>
    <!-- Chamada Quem Somos -->
<?php get_template_part('components/homepage/chamadas/chamada-quem-somos'); ?>
    <!-- Chamada 2 -->
<?php get_template_part('components/homepage/chamadas/chamada-2'); ?>
    <!-- Chamada 3 -->
<?php get_template_part('components/homepage/chamadas/chamada-3'); ?>
    <!-- Porque a Precato -->
<?php get_template_part('components/homepage/porque-a-precato/porque-a-precato'); ?>
    <!-- Chamada 4 -->
<?php get_template_part('components/homepage/chamadas/chamada-4'); ?>
    <!-- Parceiros -->
<?php get_template_part('components/homepage/parceiros/parceiros'); ?>
    <!-- Depoimentos -->
<?php get_template_part('components/depoimentos/depoimentos'); ?>
    <!-- Formulário de Contato -->
<?php get_template_part('components/formulario-contato/formulario-contato'); ?>

<?php get_footer() ?>