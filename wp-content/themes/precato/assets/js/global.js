(function ($) {

    $(document).ready(function () {
        $('[data-fancybox]').fancybox({
            autoStart: true,
        });
        $(window).scroll(function () {
            if ($(this).scrollTop() > 100) {
                $('#voltarTopo').addClass('show')
            } else {
                $('#voltarTopo').removeClass('show');
            }
        });
        $('#voltarTopo').click(function () {
            $("html, body").animate({scrollTop: 0}, 1200, 'easeInOutExpo');
            return false;
        });

        // Carousel Depoimentos
        $(".depoimentos-carousel").owlCarousel({
            loop: true,
            items: 2,
            lazyLoad: true,
            nav: false,
            dots: true,
            autoplay: true,
            autoplaySpeed: 1000,
            autoplayHoverPause: true,
            margin: 50,
            responsive: {
                0: {
                    items: 1,
                },
                600: {
                    items: 2,
                },
                1000: {
                    items: 2,
                }
            }
        });

        $(".secoes-quem-somos-carousel").owlCarousel({
            loop: false,
            items: 1,
            lazyLoad: true,
            nav: true,
            dots: false,
            autoplay: true,
            autoplaySpeed: 1000,
            autoplayHoverPause: true,
            margin: 0,
            navText: ["<img src='/precato.com.br/wp-content/themes/precato/assets/img/seta_esq.png'>","<img src='/precato.com.br/wp-content/themes/precato/assets/img/seta_dir.png'>"]
        });

        //WOW Effect
        wow = new WOW(
            {
                boxClass: 'wow',      // default
                animateClass: 'animated', // default
                offset: 0,          // default
                mobile: true,       // default
                live: true        // default
            }
        );
        wow.init();


        $(".fancybox").fancybox();

        // Mascara de CPF e CNPJ
        var CpfCnpjMaskBehavior = function (val) {
                return val.replace(/\D/g, '').length <= 11 ? '000.000.000-009' : '00.000.000/0000-00';
            },
            cpfCnpjpOptions = {
                onKeyPress: function(val, e, field, options) {
                    field.mask(CpfCnpjMaskBehavior.apply({}, arguments), options);
                }
            };

        $(function() {
            $('#cpfcnpj1, #cpfcnpj2').mask(CpfCnpjMaskBehavior, cpfCnpjpOptions);
        })
    });

    //Botão Menu Mobile
    $('.nav-toggler, .mobile-menu-overlay').click(function () {
        $('.nav-toggler').toggleClass('toggler-open');
        $('.mobile-menu-wrapper, .mobile-menu-overlay').toggleClass('open');
    });
    $('.nav-link').click(function () {
        $('.nav-toggler').removeClass('toggler-open');
        $('.mobile-menu-wrapper, .mobile-menu-overlay').removeClass('open');
    });

    $('.nav-link').removeAttr('title');


    //Contagem Porque a Precato
    $('#porque-a-precato').waypoint(function () {
        $('.count').each(function () {
            $(this).prop('Counter', 0).animate({
                Counter: $(this).text()
            }, {
                duration: 3000,
                easing: 'easeOutExpo',
                step: function (now) {
                    $(this).text(Math.ceil(now));
                }
            });
        });
        this.destroy()
    }, {offset: '70%'});

    $('.btn-choose').first().addClass('active');

    $('.btn-choose').click(function () {
        var $this = $(this);
        $siblings = $this.parent().children(),
            position = $siblings.index($this);

        $('.contentchoose .content').removeClass('active').eq(position).addClass('active');

        $siblings.removeClass('active');
        $this.addClass('active');
    });


})(jQuery);

/**
 *  maskbrphone for jQuery, version 2.0.0 (https://github.com/masimao/maskbrphone)
 *  (c) 2014 Márcio Mazzucato (https://github.com/masimao)
 *
 *  maskbrphone for jQuery is freely distributable under the terms of an MIT-style license.
 */
(function ($) {

    $.fn.maskbrphone = function (options) {

        var defaults = {
            useDdd: false,
            useDddParenthesis: true,
            dddSeparator: ' ',
            numberSeparator: '-'
        };

        var settings = $.extend({}, defaults, options);
        var plugin = this;

        var initialize = function () {

            return plugin.each(function () {
                $(this).val(applyMask($(this).val()));

                $(this).keyup(function () {
                    $(this).val(applyMask($(this).val()));
                });
            });
        };

        var applyMask = function (input) {

            if (!input.length) {
                return '';
            }

            var fone = input.replace(/\D/g, ''); // Remove tudo o que não for dígito

            var ddd = '',
                prefix,
                suffix;

            if (!settings.useDdd && input.charAt(0) === '9') {
                prefix = fone.substring(0, 5);
                suffix = fone.substring(5, 9);
            } else if (settings.useDdd && input.charAt(5) === '9') {
                ddd = fone.substring(0, 2);
                prefix = fone.substring(2, 7);
                suffix = fone.substring(7, 11);
            } else if (!settings.useDdd && input.charAt(0) !== '9') {
                prefix = fone.substring(0, 4);
                suffix = fone.substring(4, 8);
            } else if (settings.useDdd && input.charAt(5) !== '9') {
                ddd = fone.substring(0, 2);
                prefix = fone.substring(2, 6);
                suffix = fone.substring(6, 10);
            }

            if (settings.useDdd) {
                ddd = (settings.useDddParenthesis ? '(' : '') + ddd;
            }

            if (settings.useDdd && prefix.length > 0) {
                ddd += (settings.useDddParenthesis ? ')' : '') + settings.dddSeparator;
            }

            if (suffix.length > 0) {
                prefix += settings.numberSeparator;
            }

            return ddd + prefix + suffix;
        };

        initialize();

    };

    $('.telefone1, .telefone2').maskbrphone({
        useDdd: true,
        useDddParenthesis: true,
        dddSeparator: ' ',
        numberSeparator: '-'
    });

}(jQuery));