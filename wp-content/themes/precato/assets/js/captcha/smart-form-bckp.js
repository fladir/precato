jQuery(function() {
    /* @reload captcha
     ------------------------------------------- */
    function reloadCaptcha(){
        var url = jQuery("#captcha").attr("src");
        url = url.split('?');
        jQuery("#captcha").attr("src",url[0]+"?action=generate-form-captcha&r=" + Math.random());
    }
    jQuery("#tel").mask("(99) 99999-999?9");
    jQuery("#cel").mask("(99) 99999-999?9");
    jQuery('.captcode').click(function(e){
        e.preventDefault();
        reloadCaptcha();
    });
    jQuery("#smart-form").validate({
        /* @validation states + elements
         ------------------------------------------- */
        errorClass: "state-error",
        validClass: "state-success",
        errorElement: "em",
        onkeyup: false,
        onclick: false,
        /* @validation rules
         ------------------------------------------ */
        rules: {
            nome: {
                required: true,
                minlength: 2
            },
            mail: {
                required: true,
                email: true
            },
            tel: {
                required: true,
                minlength: 4
            },
            Vaga: {
                required: true,
            },
            msg: {
                required: true,
                minlength: 10
            },
            securitycode:{
                required:true
            }
        },
        messages:{
            nome: {
                required: 'Este campo é obrigatório.'
            },
            mail: {
                required: 'Este campo é obrigatório.'
            },
            tel: {
                required: 'Este campo é obrigatório.'
            },
            msg: {
                required: 'Este campo é obrigatório.'
            },
            securitycode:{
                required: 'Por favor digite o código de segurança'
            }
        },
        /* @validation highlighting + error placement
         ---------------------------------------------------- */
        highlight: function(element, errorClass, validClass) {
            jQuery(element).closest('.field').addClass(errorClass).removeClass(validClass);
        },
        unhighlight: function(element, errorClass, validClass) {
            jQuery(element).closest('.field').removeClass(errorClass).addClass(validClass);
        },
        errorPlacement: function(error, element) {
            if (element.is(":radio") || element.is(":checkbox")) {
                element.closest('.option-group').after(error);
            } else {
                error.insertAfter(element.parent());
            }
        },
        /* @ajax form submition
         ---------------------------------------------------- */
        submitHandler:function(form) {
            jQuery(form).ajaxSubmit({
                beforeSubmit:function(){
                    jQuery('.msgs-formulario').html('<div class="alert alert-light" role="alert"> <i class="fa fa-spinner fa-pulse"></i> AGUARDE!<br></div>');
                    jQuery("input").prop("disabled", true);
                    jQuery("textarea").prop("disabled", true);
                    jQuery("button").prop("disabled", true);
                    jQuery("select").prop("disabled", true);
                },
                error:function(r){
                    jQuery('.msgs-formulario').html(r);
                    jQuery('.alert').remove();
                    jQuery("input").prop("disabled", false);
                    jQuery("textarea").prop("disabled", false);
                    jQuery("button").prop("disabled", false);
                    jQuery("select").prop("disabled", false);
                },
                success:function(r){
                    jQuery('.alert').remove();
                    jQuery("input").prop("disabled", false);
                    jQuery("textarea").prop("disabled", false);
                    jQuery("button").prop("disabled", false);
                    jQuery("select").prop("disabled", false);
                    if(r){
                        jQuery("input").val("");
                        jQuery("textarea").val("");
                        jQuery('.msgs-formulario').html('<div class="alert alert-success" role="alert">MENSAGEM ENVIADA</div>');
                    }else{
                        jQuery('.msgs-formulario').html('<div class="alert alert-danger" role="alert">INFORMAÇÕES INCOMPLETAS OU ERRADAS<br></div>');
                    }
                    reloadCaptcha();
                }
            },'json');
        }
        // end submitHandler:
    });
});
    