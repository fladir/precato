<?php
/* Template Name: Precatórios */
get_header();
?>

    <!-- Topo -->
<?php  get_template_part('components/page-title/page-title'); ?>


    <!-- Passos -->
<?php  get_template_part('components/precatorios/passos'); ?>

    <!-- Perguntas Frequentes -->
<?php  get_template_part('components/precatorios/faq/faq'); ?>


    <!-- Formulário de Contato -->
<?php get_template_part('components/formulario-contato/formulario-contato'); ?>


<?php get_footer() ?>