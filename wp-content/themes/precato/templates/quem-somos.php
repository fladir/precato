<?php
/* Template Name: Quem Somos */
get_header();
?>

    <!-- Topo -->
<?php  get_template_part('components/page-title/page-title'); ?>
    <!-- Seções -->
<?php  get_template_part('components/quem-somos/sections'); ?>
    <!-- Formulário de Contato -->
<?php get_template_part('components/formulario-contato/formulario-contato'); ?>


<?php get_footer() ?>