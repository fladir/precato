<?php
/* Template Name: Calculadora de Precatórios */
get_header();
?>

    <!-- Topo -->
<?php get_template_part('components/page-title/page-title'); ?>
    <!-- Formulário -->
    <section id="form-calc">
        <div class="container">
            <div class="row">
                <div class="col-10 offset-md-1">
                    <h2 class="text-center">
                        <?php echo get_field('titulo_do_formulario'); ?>
                    </h2>
                    <?php echo do_shortcode('[contact-form-7 id="778" title="Calculadora de Precatórios"]'); ?>
                </div>
            </div>
        </div>
    </section>
    <!-- Vídeos -->
<?php get_template_part('components/calculadora-de-precatorios/videos'); ?>


<?php get_footer() ?>