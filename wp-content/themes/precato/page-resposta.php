<?php
if (!isset($_SESSION)) session_start();
if (!$_POST) {
    header('Location:' . get_site_url());
    exit;
}

/* INÍCIO: VARIÁVEIS */
$optionsACF = get_fields('options')['grupo_de_configuracoes_gerais'];
$config = $optionsACF['config_smtp'];
if (!isset($_POST['teste_smtp'])) {
    /* E-MAIL QUE RECEBERÁ O E-MAIL */
    if (isset($_POST["departamento"])) {
        $receiver_email = $_POST["departamento"];
    } elseif (isset($_POST["formulario"]) && $_POST["formulario"] == 'contato') {
        $receiver_email = $config['mail_do_destinatario'];
    } else {
        $receiver_email = $config['mail_do_destinatario'];
    }
    /* USUÁRIO: NOME */
    $sender_name = 'O cliente não preencheu o nome';
    if (isset($_POST["nome"])) {
        $sender_name = strip_tags(trim($_POST["nome"]));
    }
    /* USUÁRIO: E-MAIL */
    $sender_email = 'o-cliente-nao-preencheu@o-email.com.br';
    if (isset($_POST["email"])) {
        $sender_email = strip_tags(trim($_POST["email"]));
    }
    /* USUÁRIO: TELEFONE */
    $sender_tel = 'O cliente não preencheu o telefone';
    if (isset($_POST["tel"])) {
        $sender_tel = strip_tags(trim($_POST["tel"]));
    }
    /* USUÁRIO: MENSAGEM */
    $sender_mensagem = 'O cliente não redigiu uma mensagem.';
    if (isset($_POST["mensagem"])) {
        $sender_mensagem = strip_tags(trim($_POST["mensagem"]));
    }
    /* FINAL: VARIÁVEIS */
    $errors = array();
    if (isset($_POST["securitycode"])) {
        $securitycode = strip_tags(trim($_POST["securitycode"]));
        if (!$securitycode) {
            $errors[] = "Você precisa digitar um codigo de segurança.";
            echo 0;
            exit;
        } else if (md5($securitycode) != $_SESSION['smartCheck']['securitycode']) {
            $errors[] = "O código de segurança digitado é invalido.";
            echo 0;
            exit;
        }
    } else {
        $errors[] = "Silence is gold.";
        echo 0;
        exit;
    }
    //$order_file1 = uniqid();
    $order_file1 = $_POST['nome'];
    $order_upload1 = $order_file1 . $_FILES['curriculo']['name'];
    if ($_FILES['curriculo']['error'] == 0) {
        move_uploaded_file($_FILES['curriculo']['tmp_name'], TEMPLATEPATH . '/includes/upload-emails/' . $order_upload1);
    }
}
if ($errors) {
    $errortext = "";
    foreach ($errors as $error) {
        $errortext .= '<li>' . $error . "</li>";
    }
    echo 0;
    exit;
} else {
    require "includes/captcha/PHPMailerAutoload.php";
    if (!isset($_POST['teste_smtp'])) {
        require "includes/captcha/smartmessage.php";
    }
    $mail = new PHPMailer();
    $mail->isSendmail();
    $mail->IsHTML(true);
    $mail->IsSMTP(); // Define que a mensagem será SMTP
    $mail->Host = $config['host']; // Endereço do servidor SMTP (caso queira utilizar a autenticação, utilize o host smtp.seudomínio.com.br)
    $mail->SMTPAuth = true; // Usar autenticação SMTP (obrigatório para smtp.seudomínio.com.br)
    $mail->Username = $config['usuario_smtp']; // Usuário do servidor SMTP (endereço de email)
    $mail->Password = $config['senha_smtp']; // Senha do servidor SMTP (senha do email usado)
    $mail->Port = $config['porta_smtp']; // Senha do servidor SMTP (senha do email usado)
    $mail->SMTPSecure = $config['seguranca_smtp']; // Senha do servidor SMTP (senha do email usado)
    $mail->SMTPDebug = 0;
    $mail->From = $config["usuario_smtp"];
    $mail->CharSet = "UTF-8";
    $mail->FromName = $sender_name;
    $mail->Subject = 'Contato do Site';
    $mail->Encoding = "base64";
    $mail->Timeout = 200;
    $mail->ContentType = "text/html";
    $mail->AddReplyTo($sender_email, $sender_name);
    $mail->addAddress($receiver_email, $sender_name);
    if (isset($_POST["formulario"]) && $_POST["formulario"] == 'trabalhe-conosco') {
        $mail->AddAttachment(TEMPLATEPATH . '/includes/upload-emails/' . $order_upload1);
    }
    if (!isset($_POST['teste_smtp'])) {
        $mail->Body = $message;
    } else {
        $mail->Body = 'Email Teste SMTP';
    }
    $mail->AltBody = "Use an HTML compatible email client";
    if ($mail->Send()) {
        $msg = false;
        if (isset($_POST["formulario"]) && $_POST["formulario"] == 'trabalhe-conosco') {
            $files = glob(TEMPLATEPATH . '/includes/upload-emails/*');
            foreach ($files as $file) {
                if (is_file($file)) {
                    unlink($file);
                }
            }
        }
        $id_lead = wp_insert_post(
            array(
                'post_title' => $sender_name,
                'post_type' => 'leads'
            )
        );
        if ($id_lead) {
            update_field('email', $sender_email, $id_lead);
            update_field('telefone', $sender_tel, $id_lead);
            update_field('mensagem', $sender_mensagem, $id_lead);
            update_field('data', date('d/m/Y H:i:s'), $id_lead);
        }
        echo 1;
        exit;
    } else {
        $msg = true;
        echo 0;
        exit;
    }
}
echo $msg;
exit;