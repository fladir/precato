<?php get_header(); ?>

    <!-- Post -->
    <section id="blog">
        <article>
            <div class="container">
                <div class="row">
                    <div class="col-md-10 offset-md-1 content-post">
                        <div class="img-post-principal">
                            <h1 class="text-center"><?php the_title(); ?></h1>
                            <div class="post-data d-flex justify-content-center align-items-center mb-4 mt-4">
                                <i class="fas fa-user-circle mr-1"></i><?php the_author(); ?>
                                <i class="far fa-calendar-alt mr-1 ml-4"></i><?php the_date(); ?>
                                <i class="far fa-clock mr-1 ml-4"></i><?php the_time(); ?>
<!--                                <i class="far fa-comment-dots mr-1 ml-4"></i>--><?php //comments_number('nenhum comentário', '1 comentário', '% comentários'); ?>
                            </div>
                            <?php if (has_post_thumbnail()) : ?>
                                <?php the_post_thumbnail('blog-destaque', array('alt' => '' . get_the_title() . '', 'title' => '' . get_the_title() . '')); ?>
                            <?php endif; ?>
                        </div>


                        <?php the_content(); ?>

                    </div>
                    <div class="col-md-10 offset-md-1 author-data d-flex align-items-center">
                        <div class="author-avatar">
                            <?php if (($avatar = get_avatar(get_the_author_meta('ID'))) !== FALSE): ?>
                                <?php echo $avatar; ?>
                            <?php endif; ?>
                        </div>
                        <div class="author-info ml-4">
                            <p class="author-name"><?php the_author(); ?></p>
                            <p class="author-description"><?php echo nl2br(get_the_author_meta('description')); ?></p>
                        </div>
                    </div>
                </div>
            </div>
        </article>

    </section>

<?php get_footer(); ?>