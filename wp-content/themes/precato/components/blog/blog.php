<?php
$args = array(
    'nopaging' => false,
    'paged' => $paged,
    'post_type' => 'post',
    'posts_per_page' => 9,
    'orderby' => 'post_date',
    'order' => 'DESC'
);

$WPQuery = new WP_Query($args);
?>
    <!-- Topo -->
<?php get_template_part('components/page-title/page-title'); ?>
    <section id="blog">

        <div class="container">

            <div class="row">


                <?php if ($WPQuery->have_posts()) : while ($WPQuery->have_posts()) : $WPQuery->the_post(); ?>

                    <div class="col-md-4 position-relative wow fadeIn" data-wow-delay=".3s">


                        <div class="card mb-4">
                            <figure>
                                <a href="<?php echo get_the_permalink() ?>">
                                    <?php the_post_thumbnail('img_post_list', array('alt' => '' . get_the_title() . '', 'title' => '' . get_the_title() . '')); ?>
                                </a>
                            </figure>
                            <div class="card-body">
                                <a class="link-titulo-post" href="<?php echo get_the_permalink() ?>">
                                    <h2 class="text-left truncate3"><?php echo get_the_title() ?></h2>
                                </a>
                                <p class="truncate4"><?php echo substr(get_the_excerpt(), 0, 200); ?>...</p>
                                <a class="blog-btn" href="<?php echo get_the_permalink() ?>">Leia
                                    Mais</a>

                            </div>
<!--                            <div class="card-footer">-->
<!--                                --><?php //the_date(); ?>
<!--                            </div>-->
                        </div>

                    </div>

                <?php endwhile; endif; ?>
                <div class="col-12">
                    <div class="practice-v3-paginat text-center">
                        <?php echo bootstrap_pagination($WPQuery); ?>
                    </div>
                </div>

            </div>

        </div>

    </section>
<?php /* Restore original Post Data */
wp_reset_postdata(); ?>