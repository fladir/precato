<?php
$secoes = get_field('secoes_quem_somos');
#echo '<pre>'; print_r($secoes); echo '</pre>';
?>
<?php if ($secoes) : foreach ($secoes as $secao) : ?>

    <section id="sections-quem-somos">
        <div class="container">
            <div class="row mb-3">
                <div class="col-md-6">
                    <h2><?php echo $secao['titulo'] ?></h2>
                </div>
                <div class="col-md-6">
                    <p><?php echo $secao['texto'] ?></p>
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <?php
                    $images = $secao['imagens'];
                    if ($images): ?>
                        <div class="owl-carousel owl-theme secoes-quem-somos-carousel">
                            <?php foreach ($images as $image): ?>
                                <div class="item">
                                        <img src="<?php echo esc_url($image['sizes']['gal_quem_somos']); ?>"
                                             alt="<?php echo esc_attr($image['alt']); ?>"/>
                                </div>
                            <?php endforeach; ?>
                        </div>
                    <?php endif; ?>
                </div>
            </div>

        </div>
    </section>
<?php endforeach; endif; ?>

