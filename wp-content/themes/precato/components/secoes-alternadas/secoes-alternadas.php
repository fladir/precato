<?php
$secoesAlternadas = get_field('secoes_alternadas');
#echo '<pre>'; print_r($secoesAlternadas); echo '</pre>';
?>

<section id="Ptrainer" class="secoes-alternadas">
    <img src="<?php bloginfo('template_directory'); ?>/assets/img/particle-1.png" class="particles particle-1-bottom"/>
    <img src="<?php bloginfo('template_directory'); ?>/assets/img/particle-2.png" class="particles particle-2-bottom"/>

    <div class="container">
        <?php
        if (have_rows('secoes_alternadas')):
            $i = 0;
            while (have_rows('secoes_alternadas')) : the_row();
                $class = $i % 2 ? 'flex-md-row-reverse flex-column-reverse' : '';
                $img_side = $i % 2 ? 'right-img' : 'left-img';
                $after_bg = $i % 2 ? 'afbg-primario' : 'afbg-secundario';
                $second_img = $i == 1 ? 'second-img' : '';
                ?>
                <?php
                $imagem = get_sub_field('imagem');
                $titulo = get_sub_field('titulo');
                $texto = get_sub_field('texto');
                $textoDoBotao = get_sub_field('texto_do_botao');
                $linkDoBotao = get_sub_field('link_do_botao');
                ?>
                <div class="row <?php echo $class; ?> wow fadeInUp">
                    <div class="col-md-6">

                        <div class="wrapper-attachment-secoes-aternadas d-flex justify-content-center align-items-center ">
                            <img src="<?php echo $imagem['sizes']['secoes_alternadas'] ?>"
                                 alt="<?php echo $titulo ?>"
                                 class="attachment-secoes-aternadas <?php echo $img_side; ?> <?php echo $second_img; ?>">
                        </div>
                    </div>
                    <div class="col-md-6 mb-5 mb-md-0">
                        <h3 class="fw-bold"><?php echo $titulo ?></h3>
                        <?php echo $texto ?>
                        <?php if ($textoDoBotao) : ?>
                            <a href="<?php echo $linkDoBotao ?>" class="btn-skewed-sm">
                                <span><?php echo $textoDoBotao ?></span>
                            </a>
                        <?php endif; ?>
                    </div>
                </div>
                <?php
                $i++;
            endwhile;
        endif;
        ?>
    </div>
</section>

