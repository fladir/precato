<?php
$args = array(
    'nopaging' => false,
    'post_type' => 'depoimento',
    'posts_per_page' => 36,
    'order' => 'ASC'
);
$WPQuery = new WP_Query($args);
?>

<section id="depoimentos" class="wow fadeIn" data-wow-delay=".5s">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <h2 class="text-center mb-5 pb-5"><?php echo get_field('titulo_depoimentos') ?></h2>
                <div class="owl-carousel owl-theme depoimentos-carousel">
                    <?php if ($WPQuery->have_posts()) : while ($WPQuery->have_posts()) : $WPQuery->the_post();
                        ?>
                        <div class="item">

                            <?php the_post_thumbnail('depoimentos', array('class' => 'img-depomentos', 'alt' => '' . get_the_title() . '', 'title' => '' . get_the_title() . '')); ?>
                            <?php the_content() ?>
                            <h3><?php the_title() ?> - <?php echo get_field('local_do_cliente') ?></h3>


                        </div>
                    <?php

                    endwhile;
                    endif;
                    wp_reset_postdata(); ?>
                </div>
            </div>
        </div>
    </div>
</section>
