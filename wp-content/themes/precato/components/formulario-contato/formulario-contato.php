<?php
$grupoContato = get_fields('options')['grupo_informacoes_para_contato'];
?>

<section id="formulario-contato" class="wow fadeIn" data-wow-delay=".5s">
    <div class="container">
        <div class="row">
            <div class="col-md-8 offset-md-2">
                <h2 class="text-center"><?php echo $grupoContato['texto_acima_do_form'] ?></h2>
                <div class="form-contato">
                    <?php echo do_shortcode('[contact-form-7 id="255" title="Contato"]'); ?>
                </div>
            </div>
        </div>
    </div>
</section>

