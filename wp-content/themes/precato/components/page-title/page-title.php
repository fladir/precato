<?php
$grupoTopoDaPaginaGeral = get_field('grupo_conteudos_dos_componentes', 'options')['imagem_de_fundo'];
?>
<section id="page-title">
    <div class="container">
        <img src="<?php bloginfo('template_directory'); ?>/assets/img/img-page-title.png" class="img-page-title"/>
        <div class="row d-flex align-items-center">
            <div class="col-md-6 text-left">
                <h1 class="text-white">
                    <?php if (is_home()) : ?>
                        Blog Precato
                    <?php else : ?>
                        <?php the_title() ?>
                    <?php endif; ?>

                </h1>
                <?php
                if (function_exists('yoast_breadcrumb')) {
                    yoast_breadcrumb('<p id="breadcrumbs">', '</p>');
                }
                ?>
            </div>
            <div class="col-md-6">
                <h2 class="text-white">
                    <?php echo get_field('texto_do_topo') ?>
                </h2>
            </div>
        </div>
    </div>
</section>