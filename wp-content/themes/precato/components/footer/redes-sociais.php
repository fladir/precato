<div class="row">
    <div class="col">
        <ul class="list-unstyled list-inline">
            <?php $redes = get_field('grupo_informacoes_para_contato', 'options')['redes_sociais'];
            foreach ($redes as $rede) :
            ?>
                <?php if ($rede['link_social']) : ?>
                    <li class="list-inline-item">
                        <a href="<?php echo $rede['link_social']; ?>" target="_blank" class="social-icon-footer"><i class="<?php echo $rede['icone_social']; ?>"></i></a>
                    </li>
                <?php endif; ?>
            <?php endforeach; ?>
        </ul>
    </div>
</div>