<?php $logos = get_field('logos_parceiros'); ?>

<section id="parceiros" class="wow fadeIn" data-wow-delay=".5s">
    <div class="container">
        <div class="row">
            <h2 class="text-center w-100 mb-5">
                <?php echo get_field('titulo_parceiros') ?>
            </h2>
            <div class="d-flex justify-content-center w-100">
                <?php if ($logos) : foreach ($logos as $logo) : ?>
                    <img src="<?php echo $logo['logo']['sizes']['parceiros'] ?>" alt="">
                <?php endforeach; endif; ?>
            </div>
        </div>
    </div>
</section>
