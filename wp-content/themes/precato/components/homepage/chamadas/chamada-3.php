<?php
$chamadas = get_field('chamadas_home');
#echo '<pre>'; print_r($chamadas); echo '</pre>';
?>

<section id="chamada-3" class="wow fadeIn" data-wow-delay=".5s">
    <div class="container">
        <div class="row">
            <div class="col-md-7">
                <?php echo $chamadas['texto_chamada_3'] ?>
            </div>
            <div class="col-md-5">
                <a class="secondary-btn-bordered" href="<?php echo $chamadas['link_do_botao_chamada_3'] ?>">
                    <?php echo $chamadas['texto_do_botao_chamada_3'] ?>
                </a>
            </div>
        </div>
    </div>
</section>

