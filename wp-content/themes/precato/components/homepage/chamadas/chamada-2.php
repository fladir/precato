<?php
$chamadas = get_field('chamadas_home');
#echo '<pre>'; print_r($chamadas); echo '</pre>';
?>

<section id="chamada-2" class="position-relative wow fadeIn" data-wow-delay=".3s">
    <img src="<?php echo $chamadas['imagem_chamada_2']['sizes']['img_chamada_2'] ?>"
         alt="<?php echo $chamadas['texto_chamada_2'] ?>">
    <div class="container">
        <div class="row">
            <div class="col-12 position-absolute">
                <h2 class="text-center text-white">
                    <?php echo $chamadas['texto_chamada_2'] ?>
                </h2>
            </div>
        </div>
    </div>
</section>

