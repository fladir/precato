<?php
$chamadas = get_field('chamadas_home');
# echo '<pre>'; print_r($chamadas); echo '</pre>';
?>

<section id="chamada-quem-somos" class="wow fadeIn">
    <div class="container">
        <div class="row">
            <div class="col-md-2 offset-md-1">
                <img src="<?php echo $chamadas['imagem_chamada_quem_somos']['sizes']['img_chamada_quem_somos'] ?>"
                     alt="">
            </div>
            <div class="col-md-7 d-flex flex-column align-items-end">
                <p class="pt-3"><?php echo $chamadas['texto_chamada_quem_somos'] ?></p>
                <a class="default-btn-bordered" href="<?php echo $chamadas['link_do_botao_chamada_quem_somos'] ?>">
                    <?php echo $chamadas['texto_do_botao_chamada_quem_somos'] ?>
                </a>
            </div>
        </div>
    </div>
</section>