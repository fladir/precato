<?php
$conteudoBanner = get_field('conteudo_do_banner');
?>
<section id="banner">
    <img src="<?php echo $conteudoBanner['imagem_do_banner']['sizes']['banner'] ?>"
         alt="" class="attachment-banner">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <h2 class="text-white">
                    <?php echo $conteudoBanner['texto_do_banner'] ?>
                </h2>
            </div>
            <div class="col-md-6 d-flex justify-content-center">
                <div class="choose-wrapper">
                    <h3 class="text-center"><?php echo $conteudoBanner['titulo_do_fomulario'] ?></h3>
                    <div Class="button-wrap">
                        <button class="btn btn-choose">
                            CPF/CNPJ
                        </button>
                        ou
                        <button class="btn btn-choose">
                            Número do Processo
                        </button>
                    </div>
                    <div class="contentchoose">
                        <div class="content content-1 active">
                            <?php echo do_shortcode('[contact-form-7 id="183" title="Consulte Precatório CPF/CPNJ"]'); ?>
                        </div>
                        <div class="content content-2">
                            <?php echo do_shortcode('[contact-form-7 id="184" title="Consulte Precatório Número do Processo"]'); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


</section>