<?php
$numeros = get_field('numeros');
?>

<section id="porque-a-precato" class="wow fadeIn" data-wow-delay=".5s">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <h2 class="text-center text-white mb-4">
                    <?php echo get_field('titulo_porque_a_precato') ?>
                </h2>
            </div>
        </div>
        <div class="row">
            <?php if($numeros) : foreach ($numeros as $numero) : ?>
            <div class="col-md numeros mb-5">
                <div class="conteudo-numeros d-flex justify-content-center flex-column">
                    <div class="item-numero d-flex justify-content-center">
                        <span class="count text-center"><?php echo $numero['numero'] ?></span>
                    </div>
                    <p class="descricao-numero text-center text-white">
                        <?php echo $numero['texto_do_numero'] ?>
                    </p>
                </div>
            </div>
        <?php endforeach; endif; ?>
        </div>
    </div>
</section>
