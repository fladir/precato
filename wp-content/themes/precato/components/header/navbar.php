<?php
$grupoContato = get_fields('options')['grupo_informacoes_para_contato'];
?>
<?php #echo'<pre>'; print_r($grupoContato); echo'</pre>'; ?>
<div id="navbarWapper">
    <nav id="navbarMenu" class="navbar navbar-expand-lg py-3" role="navigation">

        <div class="container">

            <a class="navbar-brand" href="<?php bloginfo('url'); ?>" title="">
                <?php if (is_home() || is_front_page()) : ?>
                    <h1>
                        <?php echo wp_get_attachment_image(get_field('grupo_header', 'options')['logo_colorida'], 'logo_header'); ?>
                    </h1>
                <?php else : ?>
                    <?php echo wp_get_attachment_image(get_field('grupo_header', 'options')['logo_colorida'], 'logo_header'); ?>
                <?php endif; ?>
            </a>
            <?php

            wp_nav_menu(array(
                'theme_location' => 'primary',
                'depth' => 3,
                'container' => 'div',
                'container_class' => 'navbar-collapse justify-content-end site-navbar',
                'container_id' => 'navbarNav',
                'menu_class' => 'nav navbar-nav navbar-desktop',
                'fallback_cb' => 'WP_Bootstrap_Navwalker::fallback',
                'walker' => new WP_Bootstrap_Navwalker(),
            ));


            ?>
            <div class="tel-contato text-center d-flex flex-column align-items-center">
                <i class="fas fa-phone-alt"></i>
                <a href="tel:<?php echo $grupoContato['telefone'] ?>"><?php echo $grupoContato['telefone'] ?></a>
                <span><?php echo $grupoContato['texto_abaixo_do_telefone'] ?></span>
            </div>
            <div id="navTogglerFixed" class="nav-toggler d-sm-block d-lg-none">
                <span></span>
            </div>
        </div>
        <div class="nav-toggler d-sm-block d-lg-none">
            <span></span>
        </div>
    </nav>
</div>
<?php wp_enqueue_script('headerNavPadraoJS', get_template_directory_uri() . '/components/header/scrollNavPadrao.js', array('jquery'), 1, true); ?>
<?php get_template_part('components/header/mobile_menu'); ?>

</div>
<div id="navTogglerFixed" class="nav-toggler d-sm-block d-lg-none">
    <span></span>
</div>




