(function ($) {
    $(document).ready(function () {
        $(window).resize(function () {
            let lastScrollTop = 10;

            $(window).scroll(function (event) {
                let st = $(this).scrollTop();
                if (st > lastScrollTop && st > 67){
                    $('#navbarMenu').addClass('animated slideOutUp faster');
                } else if (st < 67){
                    $('#navbarMenu').removeClass('slideOutUp fixed-top menu_sticky')
                } else {
                    $('#navbarMenu').removeClass('slideOutUp').addClass('slideInDown fixed-top menu_sticky')
                }
                lastScrollTop = st;
            });


        }).resize();
    });
})(jQuery);