<?php
$passos = get_field('passos');
?>

<section id="passos">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <h2 class="text-center">
                    <?php echo get_field('titulo_passos') ?>
                </h2>
            </div>
        </div>
        <div class="row">
            <?php
            $i = 0;
            if ($passos) : foreach ($passos as $passo) : ?>
                <div class="col-md d-flex flex-column align-items-center">
                    <div class="numero-passo">
                        <?php echo $i + 1; ?>
                    </div>
                    <h3 class="text-center"><?php echo $passo['titulo'] ?></h3>
                    <img src="<?php echo $passo['icone']['sizes']['icone_passos'] ?>"
                         alt="<?php echo $passo['titulo'] ?>">
                </div>
                <?php $i++; endforeach; endif; ?>
        </div>
    </div>
</section>
