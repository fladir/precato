<?php
$faqs = get_field('faq');
?>
<section id="faq">
    <div class="container">
        <div class="row">
            <div class="col-md-8 d-flex d-md-block justify-content-around">
                <h2>
                    <?php echo get_field('titulo_faq') ?>
                </h2>
            </div>
            <div class="col-12">
                <div id="accordion" class="toggle-duvidas">
                    <?php $i = 0;
                    foreach ($faqs as $faq) :
                        $show = $i == 0 ? 'show' : '';
                        ?>
                        <div class="card wow fadeInUp">
                            <div class="card-header" id="h-<?php echo $i + 1; ?>">
                                <h5 class="mb-0 w-100">
                                    <button class="btn btn-link d-flex justify-content-between w-100 align-items-center"
                                            data-toggle="collapse"
                                            data-target="#id-<?php echo $i + 1; ?>"
                                            aria-controls="id-<?php echo $i + 1; ?>">
                                        <?php echo $i + 1; ?>. &nbsp;&nbsp;<?php echo $faq['titulo']; ?>
                                        <div class="icon-wrapper mr-3">
                                            <i class="fas fa-sort-down"></i>
                                        </div>

                                    </button>
                                </h5>
                            </div>
                            <div id="id-<?php echo $i + 1; ?>" class="collapse <?php echo $show ?>"
                                 aria-labelledby="h-<?php echo $i + 1; ?>"
                                 data-parent="#accordion">
                                <div class="card-body">
                                    <?php if ($faq['imagem']) : ?>
                                        <img src="<?php echo $faq['imagem']['sizes']['banner']; ?>"
                                             alt="<?php echo $faq['titulo']; ?>">
                                    <?php endif; ?>
                                    <?php if ($faq['conteudo']) : ?>
                                        <?php echo $faq['conteudo']; ?>
                                    <?php endif; ?>
                                </div>
                            </div>
                        </div>
                        <?php $i++; endforeach; ?>
                </div>

            </div>
            <?php if (get_field('texto_do_botao_faq')) : ?>
                <div class="col-12 d-flex justify-content-center mt-3">
                    <?php if (get_field('texto_do_botao_faq')) : ?>
                        <a href="<?php echo get_field('link_do_botao_faq') ?>"
                           class="text-center btn-skewed-sm">
                            <span><?php echo get_field('texto_do_botao_faq') ?></span>
                        </a>
                    <?php endif; ?>
                </div>
            <?php endif; ?>
        </div>
    </div>
</section>
