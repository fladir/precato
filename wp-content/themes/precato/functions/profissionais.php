<?php
add_action('init', 'type_post_profissionais');

function type_post_profissionais() {
    $labels = array(
        'name' => _x('Profissionais', 'post type general name'),
        'singular_name' => _x('Profissional', 'post type singular name'),
        'add_new' => _x('Adicionar Novo Profissional', 'Novo Profissional'),
        'add_new_item' => __('Novo Profissional'),
        'edit_item' => __('Editar Profissional'),
        'new_item' => __('Novo Profissional'),
        'view_item' => __('Ver Profissional'),
        'search_items' => __('Procurar Profissional'),
        'not_found' =>  __('Nenhum registro encontrado'),
        'not_found_in_trash' => __('Nenhum registro encontrado na lixeira'),
        'parent_item_colon' => '',
        'menu_name' => 'Profissionais'
    );


    $args = array(
        'labels' => $labels,
        'public' => true,
        'public_queryable' => true,
        'show_ui' => true,
        'query_var' => true,
        'rewrite' => true,
        'capability_type' => 'post',
        'has_archive' => true,
        'hierarchical' => false,
        'menu_position' => null,
        'menu_icon' => 'dashicons-businessperson',
        'supports' => array('title', 'editor', 'thumbnail'),
    );

    register_post_type( 'profissionais' , $args );
    flush_rewrite_rules();
}
?>