<?php
/* Template Name: Blog */
get_header();
?>

    <!-- Topo -->
<?php  get_template_part('components/topo-da-pagina/topo-da-pagina'); ?>

<?php get_template_part('/components/blog/blog'); ?>

<?php get_footer(); ?>