<?php $mensagemHTML =
  '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
  <title>Novo Contato</title>
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  
</head>

<body style="background-color:#F8F9FA; padding:35px;">

  <table cellpadding="5" cellspacing="25" align="center" border="0" style="border-top:5px solid #ec7e00; background-color: #fff; width: 600px">
    <thead>
      <tr>
        <td colspan="1" align="center"><a href="' .  $siteCliente . '"><img width="150" src="' . $logoUrl . '"></a></td>
      </tr>
      <tr>
        <td colspan="1" align="center"><h2>Contato profissional - Acabou de chegar um contato de um profissional.</h2></td>
      </tr>
    </thead>
    <tbody>
      <tr>
        <th align="left"><h2>Dados pessoais</h2></th>
      </tr> 
      <tr>
        <th align="left"><h3>Nome: ' . $mensagem->__get("nome") . '</h3></th>        
      </tr> 
      <tr>
        <th align="left"><h3>Data do Aniversário: ' . $mensagem->__get("aniversario") . '</h3></th>        
      </tr> 
      <tr>
        <th align="left"><h3>Telefone: ' . $mensagem->__get("tel") . '</h3></th>        
      </tr>
      <tr>
        <th align="left"><h3>Celular: ' . $mensagem->__get("cel") . '</h3></th>        
      </tr>      
      <tr>
        <th align="left"><h3>Email: ' . $mensagem->__get("email") . '</h3></th>        
      </tr>
      <tr>
        <th align="left"><h2>Dados de endereço</h2></th>
      </tr>
      <tr>
        <th align="left"><h3>Endereço: ' . $mensagem->__get("endereco") . '</h3></th>        
      </tr>
      <tr>
        <th align="left"><h3>Bairro: ' . $mensagem->__get("bairro") . '</h3></th>        
      </tr> 
      <tr>
        <th align="left"><h3>Cidade: ' . $mensagem->__get("cidade") . '</h3></th>        
      </tr>
      <tr>
        <th align="left"><h3>Estado: ' . $mensagem->__get("estado") . '</h3></th>        
      </tr>
      <tr>
        <th align="left"><h2>Ocupação profissional</h2></th>
      </tr>
      <tr>
        <th align="left"><h3>Atividade profissional: ' . $mensagem->__get("atividade") . '</h3></th>        
      </tr>
      <tr>
        <th align="left"><h3>Mensagem: ' . $mensagem->__get("mensagemAtividade") . '</h3></th>        
      </tr>
      <tr>
        <th align="left"><h2>Dados bancários</h2></th>
      </tr>
      <tr>
        <th align="left"><h3>Dados da conta: ' . $mensagem->__get("contaBancaria") . '</h3></th>        
      </tr>
      <tr>
        <th align="left"><h2>Mensagem adicional</h2></th>
      </tr>    
      <tr>
        <th align="left"><h3>Mensagem: ' . $mensagem->__get("mensagemAdicional") . '</h3></th>        
      </tr>      

    </tbody>
  </table>
  <br>
  <table class="table" cellpadding="5" cellspacing="50" align="center" border="0" style="background-color: #fff; width: 600px;">
    <thead>      
      <tr>
        <td colspan="1" align="center"><h2>Powered by:</h2></td>
      </tr>
    </thead>
    <tbody>
    <tr>
      <td colspan="1" align="center"><a href="https://www.netlinks.com.br"><img width="100" src="https://www.netlinks.com.br/wp-content/themes/criacao-de-sites-seo/assets/img/logo-cia/criacao-sites.png" alt="ciaWebsites"></a></td>
    </tr>
    </tbody>
  </table>

</body>

</html>';
